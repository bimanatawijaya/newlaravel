<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home','HomeController@index');

// route user register
Route::resource('/user', 'UserController',[
    'names' => [
        'create' => 'user.insert'
    ]
]);
Route::get('/register/{referal}', 'UserController@create')->name('user.create');
Route::get('/tes', 'UserController@getnoagt');

// tutup user regis

Route::get('/mycoop', 'MyCoopController@index');
