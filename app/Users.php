<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public $timestamps = false;
    protected $table = 'form_kinarya';
    protected $fillable = [
        'nama_lengkap', 'jk', 'tempat', 'tgl_lahir', 'no_hp', 'email', 'hobi', 'alamat', 'provinsi', 'ig', 'fb',
        'film_favorit', 'alasan_ikut', 'akting', 'tinggi_badan', 'berat_badan', 'warna_kulit', 'berkacamata', 'buat_film', 'film_buatan',
        'minat_film', 'nama_ayah', 'nama_ibu', 'no_ayah', 'no_ibu', 'email_ayah', 'wali', 'no_anggota', 'nama_referal', 'creted_at','code_referal','foto','persetujuan','ketersediaan'
    ];
    // protected $guarded = ['creted_at','updated_at'];
}
