<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    public $timestamps = false;
    protected $table = 't_registrasi';
    protected $fillable = [
        'no_anggota', 'nama', 'sex', 'tempat', 'tgl_lahir', 'hp', 'email', 'cdate', 'status',
        'password', 'tgl_proses', 'mdate', 'tgl_keluar', 'tgl_lahir2', 'id_koperasi', 'nama_ref', 'na_ref', 'hp_ref', 'relasi_ref', 'ket_ref'
    ];
    // protected $guarded  = ['id_koperasi'];
}
