<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TAnggota extends Model
{
    protected $table = "t_anggota";
    protected $timestamp = false; //disabled timestamp
}
