<?php

namespace App\Http\Controllers;

// get eloquent
use App\Users;
use App\TAnggota;
use App\Register;
use DateTime;
//get config
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function index()
    {
        //get data Anggota select nama 
        $data = TAnggota::select('nama')->first();
        return view('users/index', compact('data'));
    }

    public function create($referal)
    {
        //get data t_anggota where parameter
        $result = TAnggota::select('no_anggota', 'nama')
            ->where(DB::raw('md5(no_anggota)'), $referal)->first();

        return view('users/add', compact('result'));
    }

    public function store(Request $request)
    {
        $datetime = new DateTime();
        $no_anggota = $this->getnoagt();
        // proses insert
        $data = $request->validate([
            'nama_lengkap' => 'required|min:3|max:100',
            'tempat' => 'required',
            'tgl_lahir' => 'required',
            'hobi' => 'required',
            'no_hp' => 'required|min:10|max:13',
            'alamat' => 'required',
            'email' => 'required',
            'provinsi' => 'required',
            'pasfoto' => 'required|mimes:jpeg,jpg,png',
        ]);
        // cek email validtion if email already exist
        $email = Register::where('email', $request->input('email'))->orWhere('hp', $request->input('no_hp'))->first();
        if ($email) {
            return Redirect::back()->withErrors(['Email/No Handphone Anda sudah terdaftar!']);
        } else {
            //input data table form_kinarya
            //image upload 
            $images = array();
            $file = $request->file('pasfoto');
            array_push($images, $file);
            if ($images[0] != null) {
                foreach ($images as $row) {
                    $default = "form-kinarya";
                    $file_name = rand() . '.' .$default . '.' . $row->getClientOriginalExtension();
                    $local_path = storage_path('app') . "/$file_name";
                    $move = $row->move($local_path);
                    $minio = file_get_contents($move);
                    $created = Storage::disk('minio')->put($file_name, $minio);
                    Storage::delete($file_name);
                    if ($created) {
                        $img = Storage::disk('minio')->url($file_name);
                    } else {
                        redirect()->back()->with(['danger', 'gagal upload gambar']);
                    }
                }
            }
            
            // dd($request->all());
            $data = array(
                'nama_lengkap' => $request->input('nama_lengkap'),
                'jk' => $request->input('jenkel'),
                'tempat' => $request->input('tempat'),
                'tgl_lahir' => $request->input('tgl_lahir'),
                'no_hp' => $request->input('no_hp'),
                'email' => $request->input('email'),
                'hobi' => $request->input('hobi'),
                'alamat' => $request->input('alamat'),
                'provinsi' => $request->input('provinsi'),
                'ig' => $request->input('ig'),
                'fb' => $request->input('fb'),
                'film_favorit' => $request->input('film_favorit'),
                'alasan_ikut' => $request->input('alasan_ikut'),
                'akting' => $request->input('akting'),
                'tinggi_badan' => $request->input('tinggi_badan'),
                'berat_badan' => $request->input('berat_badan'),
                'warna_kulit' => $request->input('warna_kulit'),
                'berkacamata' => $request->input('berkacamata'),
                'buat_film' => $request->input('buat_film'),
                'film_buatan' => $request->input('film_buatan'),
                'minat_film' => json_encode($request->input('minat_film')),
                'nama_ayah' => $request->input('nama_ayah'),
                'nama_ibu' => $request->input('nama_ibu'),
                'no_ayah' => $request->input('no_ayah'),
                'no_ibu' => $request->input('no_ibu'),
                'email_ayah' => $request->input('email_ayah'),
                'wali' => $request->input('wali'),
                'no_anggota' => $no_anggota,
                'nama_referal' => $request->input('nama_referal'),
                'code_referal' => $request->input('no_referal'),
                'foto' => $img,
                'persetujuan'=>$request->input('persetujuan'),
                'ketersediaan'=>$request->input('ketersediaan')
                
            );
            //input data table t_registerasi
            $data1 = array(
                'no_anggota' => $no_anggota,
                'nama' => $request->input('nama_lengkap'),
                'tempat' => $request->input('tempat'),
                'sex' => $request->input('jenkel'),
                'tgl_lahir' => $request->input('tgl_lahir'),
                'password' => '123456',
                'cdate' => $datetime->format('Y-m-d H:i:s'),
                'status' => '2',
                'hp' => $request->input('no_hp'),
                'email' => $request->input('email'),
                'tgl_proses' => $datetime->format('Y-m-d H:i:s'),
                'mdate' => $datetime->format('Y-m-d H:i:s'),
                'tgl_keluar' => $datetime->format('Y-m-d H:i:s'),
                'tgl_lahir2' => $datetime->format('Y-m-d H:i:s'),
                'id_koperasi' => 1,
                'nama_ref' => '',
                'na_ref' => '',
                'hp_ref' => '',
                'relasi_ref' => '',
                'ket_ref' => '',
            );
            $result1 = Register::create($data1);
            $result = Users::create($data);
            if ($result && $result1)
                return redirect(route('user.index'));
            else
                return redirect()->back();
        }
    }

    public function getnoagt()
    {
        $r2 = DB::table('t_utility')->select(DB::raw("CONCAT(idpuskopdit,LPAD(nourutkoperasi,3,'0')) as na1"))->first();
        $no_anggota = $r2->na1 . ".000.7xxx.xxx";
        return $no_anggota;
    }

    public function show($id)
    { }

    public function edit($id)
    { }

    public function update(Request $request, $id)
    { }

    public function destroy($id)
    { }
}
