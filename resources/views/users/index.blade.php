@extends('layouts.master')
    @section('content')
    <!-- content -->
    <div style="padding:40px">
      <div class="panel panel-info">
        <div class="panel-header custom-header">
            NOTIFICATION
        </div>
        <div class="panel-body custom-body">
            <h3>Selamat data diri anda sudah terkirim</h3>                
            <h3>Direkomendasikan oleh <span class="referensi">{{$data->nama}}</span></h3>
        </div>
      </div>
    </div>
    <!-- close content -->
    @endsection