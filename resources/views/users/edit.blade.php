@extends('layouts.master')

    @section('content')
    <!-- content -->
    <div style="padding:40px">
    <section class="content">
        <div class="box-header with-border">
          <h3 class="box-title">FORM AUDISI CREW & TALENT INDONESIA (ACTION 2019)</h3>
          <h3>KINARYA - DIVISI FILM & ENTERTAINMENT</h3> 
          <h4>FM/UB/FE/1.1/2/-</h4>
        </div>
        <form action="{{route('user.update',$data->id)}}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
          <div class="box box-warning">
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div class="panel panel-default">
              <div class="panel-body">
                <h3><span class="border-title">DATA DIRI</span></h3>
                  <div class="row">
                    <div class="col-md-6">
                      {{-- inputan tempat lahir --}}
                      <div class="form-group">
                        <label>Nama Lengkap :</label>
                      <input type='text' class='form-control' placeholder="Silahkan Masukan Nama Lengkap" name='nama_lengkap'  value="{{$data->nama_lengkap}}" required>
                        </div>
                      {{-- inputan Jenis Kelamin --}}
                      <div class="form-group">
                        <label>Jenis Kelamin :</label>
                          <select class="form-control select2" data-placeholder="Select a State" style="width: 100%;" name='jenkel'>
                            <option value='L'>Laki-laki</option>
                            <option value='P'>Perempuan</option>
                          </select>
                      </div>
                    {{-- inputan tempat lahir --}}
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type='text' class='form-control' placeholder="Contoh: Bogor" name='tempat'  value="{{$data->tempat}}" required>
                    </div>
                    {{-- inputan tanggal lahir --}}
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type='date' class='form-control' placeholder="Contoh: Bogor, 20-06-2000" name='tgl_lahir'  value="{{$data->tgl_lahir}}" required>
                    </div>
                    {{-- inputan Nomor Handphone --}}
                    <div class="form-group">
                        <label>No Handphone :</label>
                        <input type='text' class='form-control' placeholder="Silahkan Masukan No Handphone" name='no_hp' value="{{ $data->no_hp }}" required>
                    </div>
                    {{-- inputan Alamat Email --}}
                    <div class="form-group">
                        <label>Alamat Email :</label>
                        <input type='text' class='form-control' placeholder="Silahkan Masukan Alamat Email" name='email' value="{{ $data->email }}" required>
                    </div>
                    {{-- inputan Hobi --}}
                    <div class="form-group">
                        <label>Hobi :</label>
                        <input type="text" class='form-control' placeholder="Silahkan Masukan Hobi" name='hobi' value="{{$data->hobi}}" required>
                    </div>
                </div>
                {{-- bagi halaman --}}
                    <div class="col-md-6">
                    {{-- inputan Alamat Rumah --}}
                    <div class="form-group">
                       <div class="form-group">
                          <label>Alamat Rumah :</label>
                          <input type="text" class='form-control' placeholder="Silahkan Masukan Alamat Rumah " name='alamat'  value="{{$data->alamat}}" required>
                      </div>
                    </div>
                    {{-- inputan Provinsi --}}
                    <div class="form-group">
                      <label>Provinsi :</label>
                      <input type='text' class='form-control' placeholder="Silahkan Masukan Provinsi" name='provinsi'  value="{{$data->provinsi}}" required>
                    </div>
                    {{-- inputan Instagram --}}
                    <div class="form-group">
                      <label>Instagram :</label>
                      <input type='text' class='form-control' placeholder="Silahkan Masukan username Instagram" name='ig' value="{{$data->ig}}">
                    </div>
                    {{-- inputan Facebook --}}
                    <div class="form-group">
                      <label>Facebook :</label>
                      <input type='text' class='form-control' placeholder="Silahkan Masukan username Facebook" name='fb' value="{{$data->fb}}">
                    </div>
                    {{-- inputan Referensi --}}
                    <div class="form-group">
                      <label>Referensi Film Favorite :</label>
                      <input type='text' class='form-control' placeholder="Silahkan Masukan Film Favorit" name='film_favorit' value="{{$data->film_favorit}}">
                    </div>
                    {{-- inputan Alasan Ikut --}}
                    <div class="form-group">
                      <label>Alasan Mengikuti Program :</label>
                      <input type='text' class='form-control' placeholder="Silahkan Masukan Alasan Mengikuti Program" name='alasan_ikut' value="{{$data->alasan_ikut}}" > 
                    </div>
                  </div>
              </div>
          </div>
        </div>
        {{-- kolom khusus talent --}}
        <div class="panel panel-default">
            <div class="panel-body">
              <h3><span class="border-title">KOLOM KHUSUS TALENT</span></h3>
                <div class="row">
                  <div class="col-md-6">
                   <div class="form-group">
                     <div class="radio">
                        <label>Apakah Pernah Acting?</label><br>
                        {{-- memilih data chacked --}}
                        @if($data->akting == 1)
                        <label>
                            <input type="radio" name="akting" id="optionsRadios1" value="1" checked>Pernah
                        </label>
                        <label>
                            <input type="radio" name="akting" id="optionsRadios2" value="0">Belum
                        </label>
                        @else
                        <label>
                            <input type="radio" name="akting" id="optionsRadios1" value="1">Pernah
                        </label>
                        <label>
                            <input type="radio" name="akting" id="optionsRadios2" value="0" checked>Belum
                        </label>
                        @endif
                        {{-- tutup data chacked --}}
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Berat Badan :</label>
                        <input type='number' class='form-control' placeholder="Masukan Berat Badan" name='berat_badan' value="{{$data->berat_badan}}">
                    </div>
                    <div class="form-group">
                      <label>Tinggi Badan :</label>
                        <input type='number' class='form-control' placeholder="Masukan Tinggi Badan" name='tinggi_badan' value="{{$data->tinggi_badan}}"> 
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Warna Kulit :</label>
                      <select class="form-control select2" data-placeholder="Select a State" style="width: 100%;" name='warna_kulit' value="{{$data->warna_kulit}}">
                            <option value=''>---Pilih Warna Kulit :---</option>
                            <option value='Putih'>Putih</option>
                            <option value='Kuning Langsat'>Kuning Langsat</option>
                            <option value='Sawo Matang/Coklat'>Sawo Matang/Coklat</option>
                            <option value='Hitam'>Hitam</option>
                          </select>
                      </div>
                      <div class="form-group">
                        <label>Apakah Anda Berkacamata? </label><br>
                            @if($data->berkacamata == 1)
                            <label>
                                <input type="radio" name="berkacamata" id="optionsRadios1" value="1" checked>
                                    Iya
                            </label>
                            <label>
                                <input type="radio" name="berkacamata" id="optionsRadios2" value="0">
                                Tidak
                            </label>
                            @else
                            <label>
                                <input type="radio" name="berkacamata" id="optionsRadios1" value="1">
                                    Iya
                            </label>
                            <label>
                                <input type="radio" name="berkacamata" id="optionsRadios2" value="0" checked>
                                Tidak
                            </label>
                            @endif
                      </div>
                      <div class="form-group" style="margin-left:20px;">
                        <label>*Upload Foto (Pas Foto & Seluruh Badan) dengan mengirimkan email ke: <i>datatalent.kinarya@gmail.com</i> </label>
                        <label>Dengan mencantumkan No. Pendaftaran, Nama & Provinsi pada Kolom <i>Perihal email</i> dengan format : </label>
                        <label><i>No.Pendaftaran_Nama_Provinsi</i> </label>
                      </div>
                     <div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
       <div class="panel panel-default">
         <div class="panel-body"> 
            <h3><span class="border-title">KOLOM KHUSUS CREW</span></h3>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <div class="radio">
                  <label>Apakah Pernah Membuat Film?</label><br>
                    <div class="radio-space">
                        @if($data->buat_film == 1)
                            <label>
                            <input type="radio" name="buat_film" id="optionsRadios1" value="1" checked>
                                Pernah
                            </label>
                            <label>
                                <input type="radio" name="buat_film" id="optionsRadios2" value="0">
                                Belum
                            </label>
                        @else
                            <input type="radio" name="buat_film" id="optionsRadios1" value="1" checked>
                            Pernah
                            <input type="radio" name="buat_film" id="optionsRadios2" value="0">
                            Belum
                        @endif
                  </div>
                </div>  
              </div>
              <div class="form-group">
                <label>Sebutkan nama film yang pernah dibuat (ketik "TIDAK ADA" jika belum pernah):</label>
                  <input type="text" class='form-control' placeholder="Masukan Film yang pernah dibuat " name='film_buatan' value="{{$data->film_buatan}}">
              </div>
              <div class="col-md-6">
                <label>Pilihan Minat Dalam Film :</label>
                    <div class="checkbox" name="minat_film">
                        <label><input type="checkbox" name="minat_film" value="Produser ">
                        Produser
                        </label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="minat_film" value="Marketing">
                            Marketing
                        </label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="minat_film" value="Keuangan">
                            Keuangan
                        </label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="minat_film" value="Scheduling">
                            Scheduling
                        </label>
                    </div>
                </div>
            <div class="col-md-6" >
              <div class="checkbox">
                <label><input type="checkbox" name="minat_film" value="IT">
                    IT
                </label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="minat_film" value="Penulisan Naskah">
                    Penulisan Naskah
                </label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="minat_film" value="Reset / Peneliti">
                    Reset / Peneliti
                </label>
              </div>
              <div class="checkbox" name="minat_film">
                <label><input type="checkbox" name="minat_film" value="Penyutradaraan">
                    Penyutradaraan
                </label>
              </div>
            </div>
          </div>
          {{-- terusan dari kolom khusus crew --}}
            <div class="col-md-6">
                <div class="form-group">
                  <div class="col-md-6">
                    <div class="checkbox">
                      <label><input type="checkbox" name="minat_film" value="Casting">
                          Casting
                      </label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" name="minat_film" value="Animasi / StoryBoard">
                          Animasi / StoryBoard
                      </label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" name="minat_film" value="Seni Rupa / Grafis">
                          Seni Rupa / Grafis
                      </label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" name="minat_film" value="Kameramen">
                          Kameramen
                      </label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" name="minat_film" value="Lighting">
                          Lighting
                      </label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" name="minat_film" value="Sound / Suara">
                          Sound / Suara
                      </label>
                    </div>
                    <div class="checkbox" name="minat_film">
                      <label><input type="checkbox" name="minat_film" value="Art">
                          Art
                      </label>
                    </div>
                  </div>
                    <div class="col-md-6">
                    <div class="checkbox">
                    <label><input type="checkbox" name="minat_film" value="Arsitek/Tata Ruang">
                        Arsitek/Tata Ruang 
                    </label>
                    </div>
                    <div class="checkbox">
                    <label><input type="checkbox" name="minat_film" value="Wardrobe/Tata Busana">
                        Wardrobe/TataBusana
                    </label>
                    </div>
                    <div class="checkbox">
                    <label><input type="checkbox" name="minat_film" value="Make-Up/Tata Rias">
                        Make-Up/Tata Rias
                    </label>
                    </div>
                    <div class="checkbox">
                    <label><input type="checkbox" name="minat_film" value="Musik">
                        Musik
                    </label>
                    </div>
                    <div class="checkbox">
                    <label><input type="checkbox" name="minat_film" value="Editing">
                        Editing
                    </label>
                    </div>
                    <div class="checkbox">
                    <label><input type="checkbox" name="minat_film" value="Kritikus Film">
                        Kritikus Film
                    </label>
                    </div>
                </div>
            </div>
        </div>     
    </div>     
        <div class="form-group" style="margin-top:30px">
            <label><i>*NOTE / Auto Reply :</i></label>
            <label><i>Terima kasih untuk ketersediaan Anda mendaftar sebagai CREW untuk kebutuhan pembuatan Film Divisi Kinarya. Silahkan Follow informasi proses audisi lebih lanjut, dapat di ikuti melalui IG & FB. Salam</i></label>
            <label><i>#WeChange, KINARYA</i></label>
        </div>
    </div>
</div>
</div>
  {{-- tutup kolom khusus catring --}}
   {{-- kolom khusus orang tua --}}
    <div class="panel panel-default">
        <div class="panel-body">
            <h3><span class="border-title">KOLOM KHUSUS PERSETUJUAN ORANG TUA</span></h3>
            <h4 class="box-title"><i>Surat Pernyataan Orang Tua</i></h4>
        <div class="col-md-6">
          <div class="form-group">
            <div class="form-group">
              <label>NAMA AYAH / WALI :</label>
              <input type='text' class='form-control' placeholder="Masukan Nama Ayah / Wali" name='nama_ayah' value="{{$data->nama_ayah}}">
            </div>
          </div>
          <div class="form-group">
            <label>NAMA IBU / WALI :</label>
            <input type='text' class='form-control' placeholder="Masukan Nama Ibu / Wali" name='nama_ibu' value="{{$data->nama_ibu}}">
          </div>
          <div class="form-group">
            <label>NO HANDPHONE AYAH / WALI :</label>
            <input type='text' class='form-control' placeholder="Silahkan Masukan No Handphone Ayah / Wali" name='no_ayah' value="{{$data->no_ayah}}">
          </div>
          <div class="form-group">
            <label>NO HANDPHONE IBU / WALI :</label>
            <input type='text' class='form-control' placeholder="Silahkan Masukan No Handphone Ibu / Wali" name='no_ibu' value="{{$data->no_ibu}}">
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <div class="form-group">
              <label>ALAMAT EMAIL AYAH / IBU</label>
                <input type="text" class='form-control' placeholder="Silahkan Masukan Email Ayah / Ibu" name='email_ayah' value="{{$data->email_ayah}}">
              </div>
             <div class="form-group">
              <label>SEBAGAI ORANG TUA / WALI DARI :</label>
                <input type="text" class='form-control' placeholder="Silahkan Masukan Sebagai Orang Tua / Wali " name='wali' value="{{$data->wali}}">
              </div>
            </div>
            <div class="form-group">
              <label>NOMOR ANGGOTA:</label>
                <input type='text' class='form-control' placeholder="Silahkan Masukan Nomor Anggota" name='no_anggota' value="{{$data->no_anggota}}">
            </div>
          <div>
        </div>
    </div>
  </div>
</div>

  <input type='submit' name="submit" class='btn btn-primary' value="Kirim data diri">
</form>
    <!-- /.box-body -->
<!-- /.box -->
</section>
    <!-- close content -->
@endsection