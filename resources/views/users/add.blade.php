@extends('layouts.master')

@section('content')
<!-- content -->
<div style="padding:40px">
  <section class="content">

    {{-- nama form --}}
    <div class="row">
      <div class="col-md-8 ">
        <h3 class="inline-style">FORM AUDISI CREW & TALENT INDONESIA (ACTION 2019)</h3>
        <h3>KINARYA - DIVISI FILM & ENTERTAINMENT</h3>
        <h4>FM/UB/FE/1.1/2/-</h4>
      </div>
      <div class="col-md-4">
        <h3>Referensi</h3>
        <h4>Dari :<span class="referensi">{{$result->nama}}</span></h4>
      </div>
    </div>
    {{-- tutup nama form --}}
    <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data">
      <div class="box box-warning">
        {{-- cek validation --}}
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        {{-- tutup cek validation --}}
        {{-- pengisian data diri --}}
        <div class="panel panel-default">
          <div class="panel-body">
            <h3><span class="border-title">DATA DIRI</span></h3>
            <div class="row">
              <div class="col-md-6">{{-- grid col-md-6 --}}
                {{-- hidden input --}}
                <div class="form-group">
                  <input type='hidden' class='form-control' name='no_referal' value="{{$result->no_anggota}}">
                  <input type='hidden' class='form-control' name='nama_referal' value="{{$result->nama}}">
                </div>
                {{-- inputan tempat lahir --}}
                <div class="form-group">
                  <label>Nama Lengkap :</label>
                  <input type='text' class='form-control' name='nama_lengkap' value="{{old('nama_lengkap')}} "
                    placeholder="Silahkan Masukan Nama Lengkap" required>
                </div>
                {{-- inputan Jenis Kelamin --}}
                <div class="form-group">
                  <label>Jenis Kelamin :</label>
                  <select class="form-control select2" data-placeholder="Select a State" style="width: 100%;"
                    name='jenkel'>
                    <option value='L'>Laki-laki</option>
                    <option value='P'>Perempuan</option>
                  </select>
                </div>
                {{-- inputan tempat lahir --}}
                <div class="form-group">
                  <label>Tempat Lahir</label>
                  <input type='text' class='form-control' placeholder="Contoh: Bogor" name='tempat'
                    value="{{old('tempat')}}" required>
                </div>
                {{-- inputan tanggal lahir --}}
                <div class="form-group">
                  <label>Tanggal Lahir</label>
                  <input type='date' class='form-control' placeholder="Contoh: Bogor, 20-06-2000" name='tgl_lahir'
                    value="{{old('tgl_lahir')}}" required>
                </div>
                {{-- inputan Nomor Handphone --}}
                <div class="form-group">
                  <label>No Handphone :</label>
                  <input type='number' class='form-control' placeholder="Silahkan Masukan No Handphone" name='no_hp'
                    value="{{ old('no_hp') }}" required>
                </div>
                {{-- inputan Alamat Email --}}
                <div class="form-group">
                  <label>Alamat Email :</label>
                  <input type='email' class='form-control' placeholder="Silahkan Masukan Alamat Email" name='email'
                    value="{{ old('email') }}" required>
                </div>
                {{-- inputan Hobi --}}
                <div class="form-group">
                  <label>Hobi :</label>
                  <input type="text" class='form-control' placeholder="Silahkan Masukan Hobi" name='hobi'
                    value="{{old('hobi')}}" required>
                </div>
              </div>


              <div class="col-md-6">{{-- grid col-md-6 --}}
                {{-- inputan Alamat Rumah --}}
                <div class="form-group">
                  <div class="form-group">
                    <label>Alamat Rumah :</label>
                    <input type="text" class='form-control' placeholder="Silahkan Masukan Alamat Rumah " name='alamat'
                      value="{{old('alamat')}}" required>
                  </div>
                </div>
                {{-- inputan Provinsi --}}
                <div class="form-group">
                  <label>Provinsi :</label>
                  <input type='text' class='form-control' placeholder="Silahkan Masukan Provinsi" name='provinsi'
                    value="{{old('provinsi')}}" required>
                </div>
                {{-- inputan Instagram --}}
                <div class="form-group">
                  <label>Instagram :</label>
                  <input type='text' class='form-control' placeholder="Silahkan Masukan username Instagram" name='ig'
                    value="{{old('ig')}}">
                </div>
                {{-- inputan Facebook --}}
                <div class="form-group">
                  <label>Facebook :</label>
                  <input type='text' class='form-control' placeholder="Silahkan Masukan username Facebook" name='fb'
                    value="{{old('fb')}}">
                </div>
                {{-- inputan Referensi --}}
                <div class="form-group">
                  <label>Referensi Film Favorite :</label>
                  <input type='text' class='form-control' placeholder="Silahkan Masukan Film Favorit"
                    name='film_favorit' value="{{old('film_favorit')}}">
                </div>
                {{-- inputan Alasan Ikut --}}
                <div class="form-group">
                  <label>Alasan Mengikuti Program :</label>
                  <input type='text' class='form-control' placeholder="Silahkan Masukan Alasan Mengikuti Program"
                    name='alasan_ikut' value="{{old('alasan_ikut')}}">
                </div>
              </div>
            </div>
          </div>
        </div>
        {{-- tutup pengisian data diri --}}
        {{-- kolom khusus talent --}}
        <div class="panel panel-default">
          <div class="panel-body">
            <h3><span class="border-title">KOLOM KHUSUS TALENT</span></h3>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="radio">
                    <label>Apakah Pernah Acting?</label><br>
                    <label>
                      <input type="radio" name="akting" id="optionsRadios1" value="1" checked>
                      Pernah
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="akting" id="optionsRadios2" value="0">
                      Belum
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label>Berat Badan :</label>
                  <input type='number' class='form-control' placeholder="Masukan Berat Badan" name='berat_badan'
                    value="{{old('berat_badan')}}">
                </div>
                <div class="form-group">
                  <label>Tinggi Badan :</label>
                  <input type='number' class='form-control' placeholder="Masukan Tinggi Badan" name='tinggi_badan'
                    value="{{old('tinggi_badan')}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Warna Kulit :</label>
                  <select class="form-control select2" data-placeholder="Select a State" style="width: 100%;"
                    name='warna_kulit'>
                    <option value=''>---Pilih Warna Kulit :---</option>
                    <option value='Putih'>Putih</option>
                    <option value='Kuning Langsat'>Kuning Langsat</option>
                    <option value='Sawo Matang/Coklat'>Sawo Matang/Coklat</option>
                    <option value='Hitam'>Hitam</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Apakah Anda Berkacamata? </label><br>
                  <input type="radio" name="berkacamata" id="optionsRadios1" value="1" checked>
                  Iya
                  <input type="radio" name="berkacamata" id="optionsRadios2" value="0">
                  Tidak
                </div>
                <div class="form-group" style="margin-left:20px;">
                  <label>*Upload Foto (Pas Foto & Seluruh Badan) dengan mengirimkan email ke:
                    <i>datatalent.kinarya@gmail.com</i> </label>
                  <label>Dengan mencantumkan No. Pendaftaran, Nama & Provinsi pada Kolom <i>Perihal email</i> dengan
                    format : </label>
                  <label><i>No.Pendaftaran_Nama_Provinsi</i> </label>
                </div>
                <div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{-- tutup kolom khusus talent --}}
        {{-- kolom khusus crew --}}
        <div class="panel panel-default">
          <div class="panel-body">
            <h3><span class="border-title">KOLOM KHUSUS CREW</span></h3>
            <div class="row">{{-- Row --}}
              <div class="col-md-6">
                <div class="form-group">
                  <div class="radio">
                    <label>Apakah Pernah Membuat Film?</label><br>
                    <div class="radio-space">
                      <label>
                        <input type="radio" name="buat_film" id="optionsRadios1" value="1" checked>
                        Pernah
                      </label>
                      <label>
                        <input type="radio" name="buat_film" id="optionsRadios2" value="0">
                        Belum
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Sebutkan nama film yang pernah dibuat (ketik "TIDAK ADA" jika belum pernah):</label>
                  <input type="text" class='form-control' placeholder="Masukan Film yang pernah dibuat "
                    name='film_buatan' value="{{old('film_buatan')}}">
                </div>
              </div>
            </div> {{-- close Row --}}
            <div class="row">
              <div class="col-md-3">{{--grid 3--}}
                {{-- pilihan minat film --}}
                <label>Pilihan Minat Dalam Film :</label>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Produser ">
                    Produser
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Marketing">
                    Marketing
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Keuangan">
                    Keuangan
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Scheduling">
                    Scheduling
                  </label>
                </div>

              </div>{{-- close grid 3--}}
              <div class="col-md-3">{{--grid 3--}}
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Casting">
                    Casting
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Animasi / StoryBoard">
                    Animasi / StoryBoard
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Seni Rupa / Grafis">
                    Seni Rupa / Grafis
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Kameramen">
                    Kameramen
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Lighting">
                    Lighting
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Sound / Suara">
                    Sound / Suara
                  </label>
                </div>
              </div>{{--close grid 3--}}
              <div class="col-md-3">{{--grid 3--}}
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Arsitek/Tata Ruang">
                    Arsitek/Tata Ruang
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Wardrobe/Tata Busana">
                    Wardrobe/TataBusana
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Make-Up/Tata Rias">
                    Make-Up/Tata Rias
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Musik">
                    Musik
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Editing">
                    Editing
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Kritikus Film">
                    Kritikus Film
                  </label>
                </div>
              </div>{{--close grid 3--}}
              <div class="col-md-3">{{--close grid 3--}}
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="IT">
                    IT
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Penulisan Naskah">
                    Penulisan Naskah
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Reset / Peneliti">
                    Reset / Peneliti
                  </label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="minat_film[]" value="Penyutradaraan">
                    Penyutradaraan
                  </label>
                </div>
                <div class="checkbox" name="minat_film[]">
                  <label><input type="checkbox" name="minat_film[]" value="Art">
                    Art
                  </label>
                </div>
              </div>{{--close grid 3--}}
            </div>
            <div class="form-group" style="margin-top:30px">
              <label><i>*NOTE / Auto Reply :</i></label>
              <label><i>Terima kasih untuk ketersediaan Anda mendaftar sebagai CREW untuk kebutuhan pembuatan Film
                  Divisi Kinarya. Silahkan Follow informasi proses audisi lebih lanjut, dapat di ikuti melalui IG & FB.
                  Salam</i></label>
              <label><i>#WeChange, KINARYA</i></label>
            </div>
          </div>
        </div>
      </div>
      {{-- tutup kolom khusus catring --}}
      {{-- kolom khusus orang tua --}}
      <div class="panel panel-default">
        <div class="panel-body">
          <h3><span class="border-title">KOLOM KHUSUS PERSETUJUAN ORANG TUA</span></h3>
          <h4 class="box-title"><i>Surat Pernyataan Orang Tua</i></h4>
          <div class="col-md-6">{{--  grid md-6 --}}
            <div class="form-group">
              <div class="form-group">
                <label>Nama ayah/wali :</label>
                <input type='text' class='form-control' placeholder="Masukan Nama Ayah / Wali" name='nama_ayah'
                  value="{{old('nama_ayah')}}">
              </div>
            </div>
            <div class="form-group">
              <label>Nama ibu /wali :</label>
              <input type='text' class='form-control' placeholder="Masukan Nama Ibu / Wali" name='nama_ibu'
                value="{{old('nama_ibu')}}">
            </div>
            <div class="form-group">
              <label>No handphone ayah /wali :</label>
              <input type='text' class='form-control' placeholder="Silahkan Masukan No Handphone Ayah / Wali"
                name='no_ayah' value="{{old('no_ayah')}}">
            </div>
            <div class="form-group">
              <label>No handphone ibu/wali :</label>
              <input type='text' class='form-control' placeholder="Silahkan Masukan No Handphone Ibu / Wali"
                name='no_ibu' value="{{old('no_ibu')}}">
            </div>
          </div>{{-- tutup grid md-6 --}}
          <div class="col-md-5">
            <div class="form-group">
              <div class="form-group">
                <label>Email ayah/ibu</label>
                <input type="text" class='form-control' placeholder="Silahkan Masukan Email Ayah / Ibu"
                  name='email_ayah' value="{{old('email_ayah')}}">
              </div>
              <div class="form-group">
                <label>Sebagai orang tua /wali dari :</label>
                <input type="text" class='form-control' placeholder="Silahkan Masukan Sebagai Orang Tua / Wali "
                  name='wali' value="{{old('wali')}}">
              </div>
            </div>
            <div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-body">
          <h3><span class="border-title">KOLOM PERSYARATAN</span></h3>
          <h4 class="box-title"><i>Tambahkan perlengkapan yang di butuhkan</i></h4>
          <div class="col-md-6">{{--  grid md-6 --}}
            <div class="form-group">
              <div class="form-group">
                <label>Upload Gambar</label>
                <input type='file' class='form-control' placeholder="Masukan Nama Ayah / Wali" name='pasfoto' required>
              </div>
            </div>
          </div>{{-- tutup grid md-6 --}}
          <div class="col-md-5">
              <div class="form-group">
                <b>Persetujuan ini dilakukan secara DIGITAL dan dapat dipertanggung jawabkan, KINARYA COOP tidak menerima keluhan daro ORANGTUA / WALI dimasa mendatang, jika ternyata pengisian kolom ini dilakukan TANPA sepengetahuan ORANGTUA / WALI </b><br>
                <i>Kami tidak akan memproses, jika kolom ini tidak diisi - khusus untuk peserta PELAJAR</i>
                <select class="form-control select2" data-placeholder="Select a State" style="width: 100%; margin-top:10px;"
                  name='persetujuan'>
                  <option value='setuju'>SETUJU</option>
                  <option value='tidak bersedia'>TIDAK SETUJU</option>
                </select>
              </div>
              <div class="form-group">
                <b>Dengan ini saya menyatakan bersedia mengikuti Program ACTION 2019 untuk pembekalan pembuatan 17 Film di 17 Provinsi yang akan dilakukan oleh Team KINARYA Coop. diawali dengan kesediaan mengikuti PRE-SCREENING Film Impian 1000 Pulau di Bioskop terdekat</b><br>
                <i>Mohon info, jika di tempat Anda tidak ada Bioskop</i>
                <select class="form-control select2" data-placeholder="Select a State" style="width: 100%; margin-top:10px;"
                  name='ketersediaan'>
                  <option value='bersedia'>YA, SAYA BERSEDIA</option>
                  <option value='tidak bersedia'>SAYA TIDAK BERSEDIA</option>
                  <option value='di lokasi saya tidak ada bioskop'>Di lokasi saya tidak ada bioskop</option>
                </select>
              </div>
            <div>
            </div>
          </div>
        </div>
      </div>
      {{ csrf_field() }}
      <input type='submit' name="submit" class='btn btn-primary' value="Kirim data diri">
    </form>
    <!-- /.box-body -->
    <!-- /.box -->
  </section>
  <!-- close content -->
  @endsection